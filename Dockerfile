# Build stage
FROM --platform=$BUILDPLATFORM golang:1.23 AS builder

ENV CGO_ENABLED=0

RUN mkdir -p /src
WORKDIR /src

COPY go.mod go.sum /src/
RUN --mount=type=cache,target=/root/.cache/go-build \
  --mount=type=cache,target=/go/pkg \
  go mod download

COPY . /src
RUN mkdir -p dist

ARG TARGETOS TARGETARCH
ENV GOOS=$TARGETOS
ENV GOARCH=$TARGETARCH

RUN --mount=type=cache,target=/root/.cache/go-build \
  --mount=type=cache,target=/go/pkg \
  go build -ldflags="-w -s" -o dist/plex-random-playlists cmd/plex-random-playlists/*.go

# Run stage
FROM gcr.io/distroless/static:nonroot

COPY --from=builder /src/dist /usr/local/bin

CMD ["plex-random-playlists"]
