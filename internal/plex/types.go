package plex

import "errors"

var (
	errRequestFailed         = errors.New("request failed")
	errRequestParsingFailed  = errors.New("request parsing failed")
	errResponseParsingFailed = errors.New("response parsing failed")
)

//nolint:tagliatelle
type baseAPIResponse struct {
	MediaContainer struct {
		MachineIdentifier string `json:"machineIdentifier"`
	} `json:"MediaContainer"`
}

//nolint:tagliatelle
type libraryResponse struct {
	MediaContainer struct {
		Directory []*Library `json:"Directory"`
	} `json:"MediaContainer"`
}

//nolint:tagliatelle
type containerResult struct {
	MediaContainer mediaContainer `json:"MediaContainer"`
}

//nolint:tagliatelle
type mediaContainer struct {
	Metadata   []*metadataJSON `json:"Metadata"`
	Identifier string          `json:"identifier"`
}

type tagJSON struct {
	Tag string `json:"tag"`
}

//nolint:tagliatelle
type metadataJSON struct {
	Duration       int64      `json:"duration"`
	GUID           string     `json:"guid"`
	GUIDs          []*GUID    `json:"Guids"`
	Key            string     `json:"ratingKey"`
	Media          []*Media   `json:"Media"`
	PlaylistItemID uint64     `json:"playlistItemID"`
	Title          string     `json:"title"`
	Type           string     `json:"type"`
	Genres         []*tagJSON `json:"Genre"`
	Labels         []*tagJSON `json:"Label"`
}

func (m *metadataJSON) metadata() Metadata {
	guids := m.GUIDs

	if m.GUID != "" {
		guids = append([]*GUID{{ID: m.GUID}}, guids...)
	}

	metadata := Metadata{
		Duration:       m.Duration,
		GUIDs:          guids,
		Key:            m.Key,
		Media:          m.Media,
		PlaylistItemID: m.PlaylistItemID,
		Title:          m.Title,
		Type:           m.Type,
		Genres:         make([]string, len(m.Genres)),
		Labels:         make([]string, len(m.Labels)),
	}

	for i, genre := range m.Genres {
		metadata.Genres[i] = genre.Tag
	}

	for i, role := range m.Labels {
		metadata.Labels[i] = role.Tag
	}

	return metadata
}

// Account.
type Account struct {
	ID    uint   `json:"id"`
	Title string `json:"title"`
}

// Library.
type Library struct {
	Key   string `json:"key"`
	Title string `json:"title"`
	Type  string `json:"type"`
	UUID  string `json:"uuid"`
}

// GUID.
type GUID struct {
	ID string `json:"id"`
}

// Part.
type Part struct {
	File string `json:"file"`
}

// Media.
//
//nolint:tagliatelle
type Media struct {
	Duration int64   `json:"duration"`
	Part     []*Part `json:"Part"`
}

// Metadata.
type Metadata struct {
	Duration       int64
	GUIDs          []*GUID
	Key            string
	Media          []*Media
	PlaylistItemID uint64
	Title          string
	Type           string
	uriRoot        string
	Genres         []string
	Labels         []string
}

func (m *Metadata) File() string {
	if len(m.Media) > 0 && len(m.Media[0].Part) > 0 {
		return m.Media[0].Part[0].File
	}

	return ""
}

// WebhookEvent.
type WebhookEvent struct {
	Type     string
	Metadata *Metadata
	Account  *Account
}
