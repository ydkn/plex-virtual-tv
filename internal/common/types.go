package common

import "errors"

var ErrNoLogger = errors.New("no logger")
