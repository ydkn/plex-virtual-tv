package plexplaylists

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"path/filepath"
	"time"

	"gitlab.com/ydkn/plex-random-playlists/internal/common"
	"gitlab.com/ydkn/plex-random-playlists/internal/plex"
	"go.uber.org/zap"
)

var (
	errPlexRequestFailed        = errors.New("plex request failed")
	errGeneratorFailed          = errors.New("playlist generator failed")
	errFailedToRetrieveMetadata = errors.New("failed to retrieve metadata")
)

type GeneratorConfig struct {
	Log                    *zap.SugaredLogger
	PlexURL                string
	PlexToken              string
	MaxDuration            time.Duration
	Libraries              []string
	Genres                 []string
	Labels                 []string
	Playlist               string
	MaxConsecutiveEpisodes uint64
}

type Generator struct {
	log                    *zap.SugaredLogger
	plex                   *plex.Client
	maxDuration            time.Duration
	libraries              []string
	genres                 []string
	labels                 []string
	playlist               string
	maxConsecutiveEpisodes uint64
	items                  []*plex.Metadata
	showEpisodes           map[string][]*plex.Metadata
}

func NewGenerator(config GeneratorConfig) (*Generator, error) {
	if config.Log == nil {
		return nil, common.ErrNoLogger
	}

	plexClient, err := plex.New(plex.ClientConfig{
		URL:   config.PlexURL,
		Token: config.PlexToken,
		Log:   config.Log.Named("plex-client"),
	})
	if err != nil {
		return nil, fmt.Errorf("%w: login: %s", errPlexRequestFailed, err.Error())
	}

	return &Generator{
		log:                    config.Log,
		plex:                   plexClient,
		maxDuration:            config.MaxDuration,
		libraries:              config.Libraries,
		genres:                 config.Genres,
		labels:                 config.Labels,
		playlist:               config.Playlist,
		maxConsecutiveEpisodes: config.MaxConsecutiveEpisodes,
	}, nil
}

func (generator *Generator) Run() error {
	err := generator.fetchItems()
	if err != nil {
		return err
	}

	items := generator.playlistItems()

	playlist, err := generator.getOrCreatePlaylist()
	if err != nil {
		return err
	}

	currentItems, err := generator.plex.GetPlaylistItems(playlist.Key)
	if err != nil {
		return fmt.Errorf("%w: %s", errGeneratorFailed, err.Error())
	}

	for _, item := range currentItems {
		_, err = generator.plex.RemovePlaylistItem(playlist.Key, item)
		if err != nil {
			return fmt.Errorf("%w: %s", errGeneratorFailed, err.Error())
		}

		generator.log.Debugw("removed item from playlist",
			"playlist", generator.playlist,
			"item", filepath.Base(item.File()))
	}

	_, err = generator.plex.AddPlaylistItems(playlist.Key, items)
	if err != nil {
		return fmt.Errorf("%w: %s", errGeneratorFailed, err.Error())
	}

	for _, item := range items {
		generator.log.Infow("added item to playlist",
			"playlist", generator.playlist,
			"item", filepath.Base(item.File()))
	}

	return nil
}

func (generator *Generator) includeLibrary(library *plex.Library) bool {
	for _, title := range generator.libraries {
		if library.Title == title {
			return true
		}
	}

	return false
}

func (generator *Generator) includeGenre(item *plex.Metadata) bool {
	if len(generator.genres) == 0 {
		return true
	}

	for _, i := range generator.genres {
		for _, j := range item.Genres {
			if i == j {
				return true
			}
		}
	}

	return false
}

func (generator *Generator) includeLabel(item *plex.Metadata) bool {
	if len(generator.labels) == 0 {
		return true
	}

	for _, i := range generator.labels {
		for _, j := range item.Labels {
			if i == j {
				return true
			}
		}
	}

	return false
}

func (generator *Generator) fetchLibraryItems(item *plex.Metadata) error {
	metadataSlice, err := generator.plex.GetLibraryMetadata(item.Key)
	if err != nil || len(metadataSlice) == 0 {
		return fmt.Errorf("%w: get-library-metadata(%s): %s", errFailedToRetrieveMetadata, item.Key, err.Error())
	}

	metadata := metadataSlice[0]

	if !generator.includeGenre(metadata) {
		return nil
	}

	if !generator.includeLabel(metadata) {
		return nil
	}

	generator.items = append(generator.items, metadata)

	if metadata.Type == "show" {
		generator.showEpisodes[metadata.GUIDs[0].ID] = make([]*plex.Metadata, 0)

		seasons, err := generator.plex.GetChildren(metadata.Key)
		if err != nil {
			return fmt.Errorf("%w: get-children(%s): %s", errPlexRequestFailed, metadata.Key, err.Error())
		}

		for _, season := range seasons {
			episodes, err := generator.plex.GetChildren(season.Key)
			if err != nil {
				return fmt.Errorf("%w: get-children(%s): %s", errPlexRequestFailed, season.Key, err.Error())
			}

			generator.showEpisodes[metadata.GUIDs[0].ID] = append(generator.showEpisodes[metadata.GUIDs[0].ID], episodes...)
		}
	}

	return nil
}

func (generator *Generator) fetchItems() error {
	generator.items = make([]*plex.Metadata, 0)
	generator.showEpisodes = make(map[string][]*plex.Metadata)

	libraries, err := generator.plex.GetLibraries()
	if err != nil {
		return fmt.Errorf("%w: get-libraries: %s", errPlexRequestFailed, err.Error())
	}

	for _, library := range libraries {
		if !generator.includeLibrary(library) {
			continue
		}

		libraryItems, err := generator.plex.GetLibraryContent(library.Key)
		if err != nil {
			return fmt.Errorf("%w: get-library-content(%s): %s", errPlexRequestFailed, library.Key, err.Error())
		}

		for _, item := range libraryItems {
			err = generator.fetchLibraryItems(item)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (generator *Generator) playlistItems() []*plex.Metadata {
	items := make([]*plex.Metadata, 0)
	duration := 0 * time.Second

	if len(generator.items) == 0 {
		return items
	}

	previousDuration := duration

	for generator.maxDuration > duration {
		num, _ := rand.Int(rand.Reader, big.NewInt(int64(len(generator.items))))
		idx := int(num.Int64())
		item := generator.items[idx]

		switch item.Type {
		case "movie":
			items = append(items, item)

			duration += time.Duration(item.Duration) * time.Millisecond
		case "show":
			if episodes, ok := generator.showEpisodes[item.GUIDs[0].ID]; ok {
				for _, episode := range generator.randomEpisodes(episodes, generator.maxConsecutiveEpisodes) {
					items = append(items, episode)

					duration += time.Duration(episode.Duration) * time.Millisecond
				}
			}
		}

		if duration <= previousDuration {
			break
		}

		previousDuration = duration
	}

	return items
}

func (generator *Generator) randomEpisodes(episodes []*plex.Metadata, maxEpisodes uint64) []*plex.Metadata {
	eps := make([]*plex.Metadata, 0)

	num, _ := rand.Int(rand.Reader, big.NewInt(int64(len(episodes))))
	idx := int(num.Int64())
	lastFile := ""

	for range maxEpisodes {
		if idx >= len(episodes) {
			break
		}

		episode := episodes[idx]

		file := episode.File()

		if file == "" {
			file = episode.GUIDs[0].ID
		}

		if file != lastFile {
			eps = append(eps, episode)
		}

		lastFile = file

		idx++
	}

	return eps
}

func (generator *Generator) getOrCreatePlaylist() (*plex.Metadata, error) {
	playlists, err := generator.plex.GetPlaylists()
	if err != nil {
		return nil, fmt.Errorf("%w: get-playlists: %s", errPlexRequestFailed, err.Error())
	}

	for _, playlist := range playlists {
		if playlist.Title == generator.playlist {
			return playlist, nil
		}
	}

	playlist, err := generator.plex.CreatePlaylist(generator.playlist, "video")
	if err != nil {
		return nil, fmt.Errorf("%w: create-playlist(%s): %s", errPlexRequestFailed, generator.playlist, err.Error())
	}

	return playlist, nil
}
